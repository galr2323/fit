package com.sqvat.squat.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.melnykov.fab.FloatingActionButton;
import com.sqvat.squat.R;
import com.sqvat.squat.activities.ChooseExerciseActivity;
import com.sqvat.squat.activities.UpdateSessionActivity;
import com.sqvat.squat.adapters.WorkoutAdapter;
import com.sqvat.squat.events.RoutineUpdated;
import com.sqvat.squat.events.WorkoutEdited;

import de.greenrobot.event.EventBus;


public class EditWorkoutFragment extends WorkoutFragment {

//    private long workoutId;
//    private Workout workout;
//
//
//    /**
//     * Use this factory method to create a new instance of
//     * this fragment using the provided parameters.
//     *
//     * @param workoutId Parameter 1.
//     * @return A new instance of fragment EditWorkoutFragment.
//     */
//
//    public static EditWorkoutFragment newInstance(long workoutId) {
//        EditWorkoutFragment fragment = new EditWorkoutFragment();
//        Bundle args = new Bundle();
//        args.putLong("workoutId", workoutId);
//        fragment.setArguments(args);
//        return fragment;
//    }
//    public EditWorkoutFragment() {
//        // Required empty public constructor
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            workoutId = getArguments().getLong("workoutId");
//            workout = Workout.load(Workout.class, workoutId);
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_workout, container, false);

        adapter = new WorkoutAdapter(getActivity(), workout);
        final ListView sessionsList = (ListView) view.findViewById(R.id.sessions_list);
        sessionsList.setAdapter(adapter);
        sessionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), UpdateSessionActivity.class);
                intent.putExtra("sessionId", adapter.getSessionId(position));
                startActivity(intent);
            }
        });

//        sessionsList.setLongClickable(true);
//        sessionsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent intent = new Intent(getActivity(), ConfigSessionActivity.class);
//                intent.putExtra("sessionId", adapter.getItem(position).getId());
//                startActivityForResult(intent, 0);
//                return true;
//            }
//        });


        TextView name = (TextView) view.findViewById(R.id.workout_name);
        name.append(workout.name);

        TextView info = (TextView) view.findViewById(R.id.workout_info);
        info.setText(workout.toString());


        FloatingActionButton addExercise = (FloatingActionButton) view.findViewById(R.id.add_exercise_fab);
        addExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ChooseExerciseActivity.class);
                intent.putExtra("workoutId", workout.getId());
                startActivityForResult(intent, 0);
            }
        });

        Toolbar cardToolbar = (Toolbar) view.findViewById(R.id.card_header_toolbar);
        cardToolbar.inflateMenu(R.menu.workout_card);
        cardToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if(menuItem.getItemId() == R.id.action_delete_workout){
                    workout.remove();
                    EventBus.getDefault().post(new RoutineUpdated());
                }
                return true;
            }
        });



        return view;
    }


//    @Override
//    public boolean onContextItemSelected(MenuItem item) {
//        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
//                .getMenuInfo();
//        if (item.getTitle() == "Edit Item") {
//            long rowId = info.id;
//            DialogFragment_Item idFragment = new DialogFragment_Item();
//            idFragment.show(getFragmentManager(), "dialog");
//        } else if (item.getTitle() == "Delete Item") {
//            mDbHelper.deleteItem(info.id);
//            return true;
//        }
//        return super.onContextItemSelected(item);
//    }

    public void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        adapter.update();
        EventBus.getDefault().postSticky(new WorkoutEdited());

    }


}
