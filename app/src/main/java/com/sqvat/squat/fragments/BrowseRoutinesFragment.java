package com.sqvat.squat.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sqvat.squat.R;
import com.sqvat.squat.adapters.RoutinesAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class BrowseRoutinesFragment extends Fragment {
    ListView routinesList;
    RoutinesAdapter adapter;

    public BrowseRoutinesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_browse_routines, container, false);

        routinesList = (ListView) view.findViewById(R.id.routines_list);
        adapter = new RoutinesAdapter(getActivity());
        routinesList.setAdapter(adapter);

        return view;

    }


}
