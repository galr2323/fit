package com.sqvat.squat.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;


import com.sqvat.squat.R;
import com.sqvat.squat.adapters.DetailedHistoryAdapter;
import com.sqvat.squat.data.CompletedWorkout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class DetailedHistoryAct extends ActionBarActivity {
    CompletedWorkout completedWorkout;
    DetailedHistoryAdapter adapter;
    ListView detailedHistoryLv;
    Toolbar toolbar;

    private final static String LOG_TAG = "detailed history act";
    private ShareActionProvider mShareActionProvider;
    private boolean shareReady;
    private Uri shareImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_history);

        detailedHistoryLv = (ListView) findViewById(R.id.detailed_history_lv);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        final long completedWorkoutId = intent.getLongExtra("completedWorkoutId", -1);

        Log.d(LOG_TAG, "completed workout id:  " + completedWorkoutId);

        completedWorkout = CompletedWorkout.load(CompletedWorkout.class, completedWorkoutId);

        Date time = completedWorkout.time;
        String timeStr = new SimpleDateFormat(" 'on' MMM dd yyyy").format(time);
        getSupportActionBar().setTitle(timeStr);
        getSupportActionBar().setSubtitle(completedWorkout.workout.name);

        adapter = new DetailedHistoryAdapter(this, completedWorkout);
        //ListAdapter adapter = new ArrayAdapter<CompletedSet>(this, R.layout.simple_li,R.id.li_text, completedWorkout.getCompletedSessions().get(0).getCompletedSets());
        detailedHistoryLv.setAdapter(adapter);

//        List<CompletedSession> completedSessions = completedWorkout.getCompletedSessions();

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.detailed_history, menu);
//// Locate MenuItem with ShareActionProvider
//
//
//
//
//        // Return true to display menu
//        return true;
//    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if(shareImg != null){
            //ready
            getMenuInflater().inflate(R.menu.detailed_history_with_share, menu);
            MenuItem item =  menu.findItem(R.id.menu_item_share);
            mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
            setUpShareIntent(shareImg);
        }
        else {
            //not yet ready
            getMenuInflater().inflate(R.menu.detailed_history, menu);
            new SetUpSharingTask().execute();

        }
        return super.onPrepareOptionsMenu(menu);
    }

    // Call to update the share intent
    private void setUpShareIntent(Uri uri) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/*");
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    class SetUpSharingTask extends AsyncTask<Object, Uri, Uri> {

        @Override
        protected Uri doInBackground(Object ... params) {
            detailedHistoryLv.setDrawingCacheEnabled(true);
            Bitmap bitmap = getWholeListViewItemsToBitmap();

            String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                    "/Squat";
            Log.d(LOG_TAG, "file_path: " + file_path);
            File dir = new File(file_path);
            if(!dir.exists())
                dir.mkdirs();
            File file = new File(dir, "yolo");
            FileOutputStream fOut;
            try {
                fOut = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //String url = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "title", null);

            Uri uri = Uri.fromFile(file);
            return uri;
        }

        @Override
        protected void onPostExecute(Uri result) {
            super.onPostExecute(result);
            shareImg = result;
            invalidateOptionsMenu();


        }
    }

    public Bitmap getWholeListViewItemsToBitmap() {

        ListAdapter adapter  = detailedHistoryLv.getAdapter();
        int itemscount       = adapter.getCount();
        int allitemsheight   = 0;
        List<Bitmap> bmps    = new ArrayList<Bitmap>();

        for (int i = 0; i < itemscount; i++) {

            View childView      = adapter.getView(i, null, detailedHistoryLv);
            childView.measure(View.MeasureSpec.makeMeasureSpec(detailedHistoryLv.getWidth(), View.MeasureSpec.EXACTLY),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

            childView.layout(0, 0, childView.getMeasuredWidth(), childView.getMeasuredHeight());
            childView.setDrawingCacheEnabled(true);
            childView.buildDrawingCache();
            bmps.add(childView.getDrawingCache());
            allitemsheight+=childView.getMeasuredHeight();
        }

        Bitmap bigbitmap    = Bitmap.createBitmap(detailedHistoryLv.getMeasuredWidth(), allitemsheight, Bitmap.Config.ARGB_8888);
        Canvas bigcanvas    = new Canvas(bigbitmap);

        Paint paint = new Paint();
        int iHeight = 0;

        for (int i = 0; i < bmps.size(); i++) {
            Bitmap bmp = bmps.get(i);
            bigcanvas.drawBitmap(bmp, 0, iHeight, paint);
            iHeight+=bmp.getHeight();

            bmp.recycle();
            bmp=null;
        }


        return bigbitmap;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
            return true;
        }
        else if(id == R.id.action_delete_completed_workout){
            completedWorkout.remove();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
