package com.sqvat.squat.data;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.Comparator;
import java.util.List;

/**
 * Created by GAL on 1/24/2015.
 */
@Table(name = "Routines")
public class Routine extends Model implements Comparable<Routine> {

    @Column(name = "Name")
    public String name;

    public Routine(String name) {
        super();
        this.name = name;
    }

    public Routine() {
        super();
    }

    public static List<Routine> getAll() {
        return new Select()
                .from(Routine.class)
                .execute();
    }

    public List<Workout> getWorkouts(){
        return getMany(Workout.class, "Routine");
    }

    @Override
    public int compareTo(Routine another) {
        return Comparators.NAME.compare(this, another);
    }

    public static class Comparators {
        public static Comparator<Routine> NAME = new Comparator<Routine>() {

            @Override
            public int compare(Routine o1, Routine o2) {
                return o1.name.compareTo(o2.name);
            }
        };

    }

}
