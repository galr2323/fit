package com.sqvat.squat.data;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.sqvat.squat.R;
import com.sqvat.squat.Util;

import static com.activeandroid.Cache.getContext;

@Table(name = "CompletedSets")
public class CompletedSet extends Model {

//    @Column(name = "Session")
//    public Session session;


    @Column(name = "CompletedSession")
    public CompletedSession completedSession;

    //0 based
    @Column(name = "OrderCol")
    public int order;

    @Column(name = "Weight")
    public double weight;

    @Column(name = "Reps")
    public int reps;

    public CompletedSet(){
        super();
    }

    @Override
    public String toString() {
        String unit = Util.getWeightUnit(getContext());
        if (unit.equals("kg")){
            return reps + " " + getContext().getString(R.string.text_reps) + " • " + weight + " " + unit;
        }
        else {
            double weightInLbs = weight * 2.20462;
            return reps + " " + getContext().getString(R.string.text_reps) + " • " + String.format( "%.2f", weightInLbs ) + " " + unit;
        }


    }

    public String getDetailedString(){
        return toString() + " • " + completedSession.session.rest + " " + getContext().getString(R.string.text_seconds_of_rest);
    }


}
