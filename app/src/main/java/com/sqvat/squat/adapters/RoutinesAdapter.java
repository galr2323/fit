package com.sqvat.squat.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sqvat.squat.R;
import com.sqvat.squat.data.Routine;

import java.util.Collections;
import java.util.List;

/**
 * Created by GAL on 3/1/2015.
 */
public class RoutinesAdapter extends BaseAdapter {
    private List<Routine> routines;
    private LayoutInflater inflater;

    public RoutinesAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        this.routines = Routine.getAll();
        Collections.sort(routines);
    }


    private class ViewHolder {
        TextView name;
        TextView description;
    }

    @Override
    public int getCount() {
        return routines.size();
    }

    @Override
    public Routine getItem(int position) {
        return routines.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.routine_li, null);
            holder.name = (TextView) convertView.findViewById(R.id.routine_name);
            holder.description = (TextView) convertView.findViewById(R.id.routine_description);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(getItem(position).name);
        holder.description.setText("");

        return convertView;
    }
}
